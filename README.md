# Treinamento COMP - ITAndroids 2017 #

Aqui estão documentados os arquivos referentes ao treinamento de Computação ITAndroids 2017. 
Nesse repositório, é possível encontrar:

* -Listas de exercício e teoria dos treinamentos;
* -Links contendo indicações de sites de aprofundamento;

### Instalação: 
* Inicialmente, é necessário instalar o git:
```sh
$ apt-get install git
```
* Faça o git clone do código. No canto superior esquerdo desta página, clique em **clone** e execute o comando no seu terminal. 

### Regras: 
* NÃO USE **git add .**, **git commit -a**. Se possível, adicione cada arquivo individualmente para evitar colocar modificações desnecessárias.
* Não coloque arquivos que não interessam, como arquivos .txt utilizados para debug. Isso evita que o repositório fique desorganizado.
* Evite enviar códigos com "std::cout" que você utilizou para debuggar algo.
* Por fim, **SEMPRE compile e teste o código antes de enviá-lo**.